<!DOCTYPE html>
<html>
<head>
    <title>Greetings</title>
</head>
<body>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve the input name from the form
    $name = $_POST['name'];

    if (!empty($name)) {
        
        echo "<p>Hello, $name! Welcome to our website.</p>";
    } else {
        echo "<p>Please enter your name.</p>";
    }
}
?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    Enter your name: <input type="text" name="name">
    <input type="submit" value="Submit">
</form>

</body>
</html>
